from typing import Callable, List, Optional, Union

import bokeh
import bokeh.palettes as bp
import numpy as np
import pandas as pd
from bokeh.io import output_notebook, show
from bokeh.layouts import Column
from bokeh.models import ColumnDataSource, CustomJS, Range1d, Slider
from bokeh.plotting import Figure
from bokeh.resources import Resources

from exptools import aggregators
from exptools.name_resolution import get_close_name
from exptools.utils import get_list, split_data_by_keys

PALETTE = (
    bp.Category10_10
    + bp.Dark2_8
    + tuple(np.random.RandomState(seed=1).permutation(bp.Viridis256 + bp.Plasma256))
)


def init_notebook():
    output_notebook(resources=Resources("inline"))


def lineplot(
    data: pd.DataFrame,
    x: str,
    y: Union[str, List[str]],
    split_curves_by: Optional[Union[str, List[str]]] = None,
    split_graphs_by: Optional[Union[str, List[str]]] = None,
    aggregator_fn: Callable[
        [pd.core.groupby.generic.DataFrameGroupBy], pd.DataFrame
    ] = aggregators.mean_std_aggregator,
    width: int = 1200,
    height: int = 600,
    legend_location: str = "bottom_right",
    title: str = "",
    x_ticks: Optional[bokeh.models.Ticker] = None,
    x_range=None,
    y_range=None,
) -> None:
    """Draws a cool and informative plot with your data.

    Args:
        data: DataFrame with all the data.
        x: column name to be used as x axis; typically just 'x'.
        y: one or multiple column names to be used as y values.
            In case of multiple names, multiple curves will be created.
        split_curves_by: One or more parameter across which data will be split to produce separate curves.
        split_graphs_by: One or more parameter across which data will be split to produce separate graphs.
        aggregator_fn: Aggregator function to produce point estimate and confidence intervals.
            See examples in aggregators.py.
        width: Width of a single graph.
        height: Height of a single graph.
        legend_location: Location to display the legend.
        title: Core of the title, which will be shared across produced graphs.
        x_ticks: Optional Ticker object for x axis.
    """
    # Validate column names
    x = get_close_name(x, data.columns)
    y = [get_close_name(y_, data.columns) for y_ in get_list(y)]

    split_curves_by = [
        get_close_name(h, data.columns) for h in get_list(split_curves_by)
    ]
    split_graphs_by = [
        get_close_name(h, data.columns) for h in get_list(split_graphs_by)
    ]

    graph_layouts = []

    for graph_id, graph_data in split_data_by_keys(data, split_graphs_by):
        graph_title = f"{title} {graph_id}"
        graph = Figure(plot_width=width, plot_height=height, title=graph_title)

        graph_elements = []

        color_num = 0

        for metric in y:
            graph_data_single_metric = graph_data.copy()
            graph_data_single_metric.rename(columns={x: "x", metric: "y"}, inplace=True)

            graph_data_single_metric = aggregator_fn(
                graph_data_single_metric.groupby(
                    ["x"] + split_curves_by, dropna=False
                ).y
            )

            for curve_id, curve_data in split_data_by_keys(
                graph_data_single_metric, split_curves_by
            ):
                if len(y) > 1:
                    curve_id = f"{metric}@{curve_id}"
                curve_data = curve_data[["x", "y", "y_lower", "y_upper"]]
                curve_data = curve_data.dropna()
                graph_elements.append(
                    _get_one_curve(graph, curve_data, curve_id, PALETTE[color_num])
                )
                color_num += 1

        slider_smooth = Slider(
            start=0.0, end=0.99, value=0.0, step=0.01, title="smoothing"
        )
        slider_ci = Slider(
            start=0, end=0.3, value=0.3, step=0.01, show_value=False, title="show CI"
        )

        for line_callback, ci_callback, ci_glyph in graph_elements:
            slider_smooth.js_on_change("value", line_callback)
            slider_smooth.js_on_change("value", ci_callback)
            slider_ci.js_link("value", ci_glyph, "fill_alpha")

        layout = Column(slider_smooth, slider_ci, graph)

        graph.legend.click_policy = "hide"
        graph.legend.location = legend_location
        if x_ticks:
            graph.xaxis.ticker = x_ticks
        if x_range:
            graph.x_range = Range1d(*x_range)
        if y_range:
            graph.y_range = Range1d(*y_range)

        graph_layouts.append(layout)

    show(Column(*graph_layouts))


def _get_one_curve(graph, data, legend_label="", color=None):
    data = data.copy()

    # Smoothed versions of variables
    data["y_s"] = data["y"]
    data["y_lower_s"] = data["y_lower"]
    data["y_upper_s"] = data["y_upper"]

    # Draw a line
    source = ColumnDataSource(data)
    line_renderer = graph.line(
        "x",
        "y_s",
        source=source,
        line_width=3,
        line_alpha=0.9,
        legend_label=legend_label,
        color=color,
    )
    callback_line = CustomJS(
        args=dict(source=source),
        code="""
                 var data = source.data;
                 var smooth_coeff = cb_obj.value
                 var x = data['x']
                 var y = data['y']
                 var y_s = data['y_s']

                 var value = y[0]

                 for (var i = 0; i < x.length; i++) {
                     if(isNaN(value)) {
                         value = y[i]
                     }
                     else {
                         value = (1-smooth_coeff)*y[i] + smooth_coeff*value
                         y_s[i] = value
                     }
                 }
                 source.change.emit();
             """,
    )

    # Draw confidence intervals
    ci_renderer = graph.varea(
        "x", "y_upper_s", "y_lower_s", source=source, alpha=0.3, color=color
    )

    callback_ci = CustomJS(
        args=dict(source=source),
        code="""
                 var data = source.data;
                 var smooth_coeff = cb_obj.value
                 var x = data['x']
                 var y_lower = data['y_lower']
                 var y_lower_s = data['y_lower_s']
                 var y_upper = data['y_upper']
                 var y_upper_s = data['y_upper_s']

                 var value_lower = y_lower[0]
                 var value_upper = y_upper[0]

                 for (var i = 0; i < x.length; i++) {
                     if(isNaN(value_lower)) {
                         value_lower = y_lower[i]
                     }
                     else {
                         value_lower = (1-smooth_coeff)*y_lower[i] + smooth_coeff*value_lower
                         y_lower_s[i] = value_lower
                     }

                     if(isNaN(value_upper)) {
                         value_upper = y_upper[i]
                     }
                     else {
                         value_upper = (1-smooth_coeff)*y_upper[i] + smooth_coeff*value_upper
                         y_upper_s[i] = value_upper
                     }
                 }
                 source.change.emit();
             """,
    )

    line_renderer.js_link("visible", ci_renderer, "visible")

    return callback_line, callback_ci, ci_renderer.glyph
