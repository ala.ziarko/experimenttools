import os

import neptune.new as neptune
import pandas as pd


def flatten_dict(d):
    df = pd.json_normalize(d, sep="/")
    res = df.to_dict(orient="records")[0]
    return res


def get_float_channels(run):
    # Treat new API exps differently.
    if "logs" not in run.get_structure().keys():
        channel_dict = flatten_dict(run.get_structure())
    else:
        channel_dict = flatten_dict(run.get_structure()["logs"])

    channels = [
        k
        for (k, v) in channel_dict.items()
        if type(v) == neptune.attributes.series.float_series.FloatSeries
    ]
    return channels


def get_id_from_keys_vals(keys, vals):
    assert len(keys) == len(vals)
    id = ",".join([f"{key}={val}" for (key, val) in zip(keys, vals)])
    return id


def get_list(x):
    if x is None:
        return []
    if isinstance(x, list):
        return x
    if isinstance(x, tuple):
        return list(x)
    return [x]


def get_neptune_api_token():
    if "NEPTUNE_API_TOKEN" in os.environ:
        return os.environ["NEPTUNE_API_TOKEN"]
    elif "NEPTUNE_API_TOKEN_" in os.environ:
        return os.environ["NEPTUNE_API_TOKEN_"]
    else:
        print("NEPTUNE_API_TOKEN not found")


def sort_key(x):
    if type(x) is tuple:
        res = []
        for y in x:
            res.extend([str(type(y)), y])
        return tuple(res)
    return str(type(x)), x


def split_data_by_keys(data, keys):
    if not keys:
        return [("", data)]
    res = [
        (get_id_from_keys_vals(get_list(keys), get_list(vals)), chunk)
        for (vals, chunk) in sorted(
            data.groupby(keys, dropna=False), key=lambda x: sort_key(x[0])
        )
    ]
    res = list(res)
    return res
