# exptools

A library to download data from Neptune and create interactive aggregated plots.

## Installation

For standard installation, use `pip install .`

To install in editable mode, use `pip install -e .`

To install from notebook/Colab, use

`!pip install -q git+https://gitlab.com/awarelab/experimenttools`

## Usage

Exptools provides two key features: efficiently downloading data from Neptune (`Downloader` class and `get_data()`
method), and creating interactive aggregated plots (`lineplot()` method).

All these methods have docstrings so you can check them out; also see examples below.

### Examples

One example of usage is below:

```python
from exptools.downloader import Downloader
from exptools.plotter import init_notebook, lineplot

# If you are in a notebook/colab, make sure to call the following line!
# init_notebook()

downloader = Downloader(
    organization="pmtest", project="examples", storage_path="/tmp"
)
data = downloader.get_data(tags="boring_ramanujan", channel_names=["AverageNormEpRet"])
lineplot(
    data,
    x="step",
    y="AverageNormEpRet",
    split_curves_by="gamma",
    split_graphs_by="lam"
)
```

More examples are available in [examples](examples/) folder.

See also the [colab](https://colab.research.google.com/drive/142RrEnIBiF1g5z7se62J2bqU4FmK35Ux)
with examples.

## Troubleshooting

Please make sure you have up-to-date jupyter (`pip install -U jupyter jupyter-client jupyter-console jupyter-core`) as you may suffer from this issue ["Duplicate Signature" error when writing to stdout using concurrent.futures.ProcessPool executor #541](https://github.com/jupyter/jupyter_client/issues/541).