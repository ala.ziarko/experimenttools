from exptools.downloader import Downloader

# Example of download. We assume that NEPTUNE_API_TOKEN or NEPTUNE_API_TOKEN_
# env variable is set.

database = {"boring_ramanujan": ["awesome_alias"]}
downloader = Downloader(
    organization="pmtest",
    project="examples",
    storage_path="/tmp",
    default_database=database,
)

data = downloader.get_data(tags="awes0me_alias", channel_names=["AverageNormEpRet"])
print(data.columns)
