from exptools.downloader import Downloader

# Example of download. We assume that NEPTUNE_API_TOKEN or NEPTUNE_API_TOKEN_
# env variable is set.

downloader = Downloader(organization="pmtest", project="examples", storage_path="/tmp")

data = downloader.get_data(
    tags="boring_ramanujan",
    channel_names=lambda k: "AverageNormEpRet" in k,
)
print(data.columns)
