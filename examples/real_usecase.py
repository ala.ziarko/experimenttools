from bokeh.io import output_file
from exptools.downloader import Downloader
from exptools.plotter import lineplot

# Example of download. We assume that NEPTUNE_API_TOKEN or NEPTUNE_API_TOKEN_
# env variable is set.

downloader = Downloader(organization="pmtest", project="examples", storage_path="/tmp")

output_file("real_usecase_plot.html")
data = downloader.get_data(tags="boring_ramanujan", channel_names=["AverageNormEpRet"])
lineplot(
    data, x="step", y="AverageNormEpRet", split_curves_by="gamma", split_graphs_by="lam"
)
